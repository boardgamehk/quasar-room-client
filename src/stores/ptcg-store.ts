import { defineStore } from 'pinia';

type room = {
  id: string;
  name: string;
  users: Array<string>;
};

export const usePtcgStore = defineStore('ptcg', {
  state: () => ({
    objectRef: {
      cardListObj: {
        loading: false,
        filter: '',
        nameFilter: '',
        attributeFilter: {},
        categoryFilter: {},
        packFilter: {},
        pagination: {
          sortBy: 'CardId',
          descending: false,
          page: 1,
          rowsPerPage: 10,
          rowsNumber: 1,
        },
        passwordPrompt: false,
      },
    } as object,
    list: [] as room[],
    userId: '' as string,
    userName: '' as string,
    roomName: '' as string,
    roomToken: '' as string,
    joinRoomToken: '' as string,
    createRoomPassword: '' as string,
    joinRoomPassword: '' as string,
    deckCode: '' as string,
    decks: [] as object[],
    cards: [] as object[],
    attributes: [] as object[],
    cardDataFile: null as unknown as File,
    uploadCardMessages: [] as object[],
    uploadCardProgress: 0 as number,
    uploadCardTotal: '' as string,
    uploadCardFinished: '' as string,
    patchCardMessages: [] as object[],
    patchCardProgress: 0 as number,
    patchCardTotal: '' as string,
    patchCardFinished: '' as string,
    selectedDeck: {} as object,
    selectedCard: {} as object,
    deckBuilderUiMode: 1 as number, // 1 = {Center: Deck, Right: Card}, 2 = {Center: Card, Right: Deck}
    discord: {} as object,
  }),
  getters: {
    getList: (state) => {
      return state.list;
    },
  },
  actions: {
    clearList() {
      this.list = [];
    },
    setList(roomList: room[]) {
      this.list = roomList;
    },
    pushList(item: room) {
      this.list.push(item);
      console.log(this.list)
    },
  },
});
