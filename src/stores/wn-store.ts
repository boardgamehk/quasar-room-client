import { defineStore } from 'pinia';

type room = {
  id: string;
  name: string;
  users: Array<string>;
};

export const usewnStore = defineStore('Wn', {
  state: () => ({
    list: [] as room[],
    userName: '' as string,
  }),
  getters: {
    getList: (state) => {
      return state.list;
    },
  },
  actions: {
    clearList() {
      this.list = [];
    },
    setList(roomList: room[]) {
      this.list = roomList;
    },
    pushList(item: room) {
      this.list.push(item);
      console.log(this.list)
    },
  },
});
