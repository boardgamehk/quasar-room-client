class AuthenController {
    constructor(baseUrl) {
        this.baseUrl = baseUrl
        // this.https = require('https')
        this.axios = require('axios')
        // this.axios = this.axios.create({
        //     httpsAgent: new this.https.Agent({
        //         rejectUnauthorized: false
        //     })
        // });
    }

    getJwtToken(identifier, password) {
        let url = `${this.baseUrl}/api/auth/local`
        console.log(url)
        return this.axios
            .post(url, {
                identifier: identifier,
                password: password
            })
            .then((response) => {
                // Handle success.
                // console.log(`Data: ${response.data.jwt}`);
                return response.data.jwt
            })
            .catch((error) => {
                // Handle error.
                console.log(`An error occurred: ${error.response}`);
            });
    }

    checkJwt(jwt) {
        let url = `${this.baseUrl}/api/users/me`
        console.log(url)
        return this.axios
            .get(url, {
                headers: {
                    Authorization:
                        `Bearer ${jwt}`,
                }
            })
            .then((response) => {
                console.log(JSON.stringify(response.data))
                if (
                    response?.data?.confirmed === true &&
                    response?.data?.blocked === false
                ) {
                    return true
                }
                return false
            })
            .catch((error) => {
                // Handle error.
                console.log(`An error occurred: ${JSON.stringify(error.response)}`);
                return false
            });
    }

}

module.exports = AuthenController;