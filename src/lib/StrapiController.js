/* eslint-disable @typescript-eslint/no-this-alias */
class StrapiController {
    constructor(baseUrl) {
        this.baseUrl = baseUrl
        // this.https = require('https')
        this.axios = require('axios')
        // this.axios = this.axios.create({
        //     httpsAgent: new this.https.Agent({
        //         rejectUnauthorized: false
        //     })
        // });
        this.qs = require('qs')
        // this.strapiToken = require('./StrapiToken.json')
    }

    GetDeck(deckCode, getCardData) {
        let jsonBody = {
            filters: {
                DeckCode: {
                    $eq: deckCode,
                },
            }
        }
        if (getCardData) {
            jsonBody.populate = {
                cards: {
                    populate: ['Card'],
                }
            }
        }
        let query = this.qs.stringify(jsonBody, {
            encodeValuesOnly: true,
        });
        let url = `${this.baseUrl}/api/decks?${query}`;
        console.log(url);
        return this.axios.get(url)
            .then(res => {
                if (res?.data?.meta?.pagination?.total === 1) {
                    return this.cleanUpStrapiResponse(res.data.data[0], this)
                }
            })
            .catch(error => console.error(error))
    }

    GetCard(params) {
        let jsonBody = {}
        if (params?.page != undefined && params?.pageSize != undefined) {
            jsonBody.pagination = {
                page: params?.page,
                pageSize: params?.pageSize,
            }
        }
        if (params?.limit != undefined) {
            if (jsonBody.pagination == undefined) {
                jsonBody.pagination = {}
            }
            jsonBody.pagination.limit = params?.limit
        }

        if (params?.sort != undefined) {
            jsonBody.sort = `${params.sort}:${params.desc ? 'desc' : 'asc'}`
        }
        if (params?.filter != undefined && params.filter.length > 0) {
            jsonBody.filters = {
                $or: [
                    {
                        Name: {
                            $containsi: params?.filter,
                        },
                    },
                    {
                        Attribute: {
                            $containsi: params?.filter,
                        },
                    },
                    {
                        CardId: {
                            $containsi: params?.filter,
                        },
                    },
                    {
                        Pack: {
                            $containsi: params?.filter,
                        },
                    },
                    {
                        EvolveBase: {
                            $containsi: params?.filter,
                        },
                    },
                    {
                        Category: {
                            $containsi: params?.filter,
                        },
                    },
                    {
                        PokemonType: {
                            $containsi: params?.filter,
                        },
                    },
                    {
                        Description: {
                            $containsi: params?.filter,
                        },
                    },
                ],
            }
        }
        if (params?.nameFilter != undefined && params.nameFilter.length > 0) {
            if (jsonBody.filters === undefined) {
                jsonBody.filters = {}
            }
            if (jsonBody.filters.$and === undefined) {
                jsonBody.filters.$and = []
            }
            jsonBody.filters.$and.push({
                Name: {
                    $containsi: params.nameFilter,
                },
            })
        }
        if (params?.attributeFilter != undefined && params.attributeFilter.length > 0) {
            if (jsonBody.filters === undefined) {
                jsonBody.filters = {}
            }
            if (jsonBody.filters.$and === undefined) {
                jsonBody.filters.$and = []
            }
            jsonBody.filters.$and.push({
                Attribute: {
                    $containsi: params.attributeFilter,
                },
            })
        }
        if (params?.categoryFilter != undefined && params.categoryFilter.length > 0) {
            if (jsonBody.filters === undefined) {
                jsonBody.filters = {}
            }
            if (jsonBody.filters.$and === undefined) {
                jsonBody.filters.$and = []
            }
            jsonBody.filters.$and.push({
                Category: {
                    $containsi: params.categoryFilter,
                },
            })
        }
        if (params?.packFilter != undefined && params.packFilter.length > 0) {
            if (jsonBody.filters === undefined) {
                jsonBody.filters = {}
            }
            if (jsonBody.filters.$and === undefined) {
                jsonBody.filters.$and = []
            }
            jsonBody.filters.$and.push({
                Pack: {
                    $containsi: params.packFilter,
                },
            })
        }

        let query = this.qs.stringify(jsonBody, {
            encodeValuesOnly: true,
        });

        let url = `${this.baseUrl}/api/cards?${query}`;
        console.log(url);
        return this.axios.get(url)
            .then(res => {
                return params?.getCountOnly ? res?.data?.meta?.pagination?.total : this.cleanUpStrapiResponse(res.data.data, this)
            })
            .catch(error => console.error(error))
    }

    CreateDeck(deck) {
        console.log(`Creating deck: ${deck.name}`)

        //  Need to remove id field, otherwise will fail
        deck.cards.forEach(cardObj => {
            cardObj.id = undefined
        });

        let postBody = {
            data: {
                Name: deck.name,
                DeckCode: this.generateDeckCode(this),
                cards: deck.cards
            }
        }
        // console.log(`${JSON.stringify(postBody)}`)

        let url = `${this.baseUrl}/api/decks`;
        return this.axios.post(url, postBody)
            .then(res => {
                let newDeck = this.cleanUpStrapiResponse(res.data, this)
                console.log(`Added deck: ${newDeck.Name} ${newDeck.DeckCode}`)
                return { result: 'success', data: newDeck }
            })
            .catch(error => {
                console.log(`Failed to add deck: ${postBody.data.Name}`)
                console.log(`${error}`)
                console.log(`${JSON.stringify(postBody)}`)
                return { result: 'error', data: error }
            })
    }

    GetCardCount = (filter, nameFilter, attributeFilter, categoryFilter, packFilter) => {
        let params = {
            page: 1,
            pageSize: 1,
            getCountOnly: true,
            filter: filter,
            nameFilter: nameFilter,
            attributeFilter: attributeFilter,
            categoryFilter: categoryFilter,
            packFilter: packFilter,
        }
        return this.GetCard(params)
            .then(count => {
                // console.log(JSON.stringify(count))
                return count
            })
            .catch(error => {
                console.log(error)
                return 0
            })
    }

    /**
     * It fetches all the cards from the database, then it checks if the card's CardId, Pack, Image, and
     * CardIdSearch fields are correct, and if they're not, it updates them
     * @param ref - the reference to the Vue component
     * @param jwt - the JWT token
     */
    PatchCards(ref, jwt) {
        ref.output.push('正在獲取卡片總數...')
        let self = this
        let headers = {
            headers: {
                Authorization:
                    `Bearer ${jwt}`,
            }
        }
        this.GetCardCount()
            .then(count => {
                ref.total = count
                ref.finished = 0
                ref.output.push(`卡片總數: ${ref.total}`)

                let fetched = []

                let pageSize = 100
                let totalPages = Math.ceil(count / pageSize)
                ref.output.push('正在獲取卡片資料...')
                for (let page = 1; page <= totalPages; page++) {
                    let intervalFetch = 100
                    setTimeout(() => {
                        let params = {
                            page: page,
                            pageSize: pageSize,
                        }
                        this.GetCard(params)
                            .then(cards => {
                                if (ref.status === 'done') {
                                    return
                                }
                                cards.forEach(card => {
                                    fetched.push(card)
                                })
                            })
                            .catch(error => {
                                ref.output.push(`錯誤: ${error}`)
                                ref.status = 'done'
                            })
                    }, page * intervalFetch)
                }

                let intervalCheckDone = setInterval(() => {
                    if (fetched.length >= ref.total || ref.status === 'done') {
                        ref.status = 'running'
                        clearInterval(intervalCheckDone)

                        let intervalPatch = 50
                        fetched.forEach((card, index) => {
                            setTimeout(() => {
                                let expected = {}
                                expected.CardId = card.CardId
                                expected.Pack = expected.CardId.split('_')[0]
                                expected.Image = card.Image
                                expected.CardIdSearch = `${expected.Pack}_${('000' + expected.CardId.split('_')[1]).slice(-3)}`
                                let urlPack = expected.Pack.length < 3 ? `${expected.Pack}-0` : expected.Pack
                                urlPack = urlPack.toLowerCase()
                                expected.Image = `https://ptcgworld.blob.core.windows.net/${urlPack}/${expected.CardId}.jpg`

                                if (expected.CardId !== card.CardId ||
                                    expected.Pack !== card.Pack ||
                                    expected.Image !== card.Image ||
                                    expected.CardIdSearch !== card.CardIdSearch) {

                                    let putBody = {
                                        data: {
                                            CardId: expected.CardId,
                                            Pack: expected.Pack,
                                            Image: expected.Image,
                                            CardIdSearch: expected.CardIdSearch,
                                        }
                                    }
                                    let url = `${self.baseUrl}/api/cards/${card.id}`;
                                    self.axios.put(url, putBody, headers)
                                        .then(res => {
                                            let updatedCard = self.cleanUpStrapiResponse(res.data, self)
                                            // console.log(`Updated card: ${updatedCard.CardId} ${updatedCard.Name}`)
                                            ref.output.push(`已更新卡片: ${updatedCard.CardId} ${updatedCard.Name}`)
                                        })
                                        .catch(error => {
                                            // console.log(`Failed to update card: ${CardId} ${Name}`)
                                            // console.log(`${error}`)
                                            // console.log(`${JSON.stringify(putBody)}`)
                                            ref.output.push(`無法更新卡片: ${CardId} ${Name}\n錯誤: ${error}\nPostBody: ${JSON.stringify(putBody)}`)
                                        })
                                        .finally(() => {
                                            ref.finished += 1
                                            ref.progress = ref.finished / ref.total

                                            if (ref.finished >= ref.total) {
                                                ref.status = 'done'
                                            }
                                        })
                                } else {
                                    ref.finished += 1
                                    ref.progress = ref.finished / ref.total
                                    ref.output.push(`卡片資料正常: ${card.CardId} ${card.Name}`)

                                    if (ref.finished >= ref.total) {
                                        ref.status = 'done'
                                    }
                                }
                            }, index * intervalPatch)
                        })

                    }
                }, 100)
            })
            .catch(error => {
                ref.output.push(`錯誤: ${error}`)
                ref.status = 'done'
            })
    }

    ImportCardFromTxt(ref, jwt) {
        let cards = []
        let card = ''
        let lines = ref.input.split('\n');
        lines.forEach(line => {
            if (line.trim().length > 0) {
                card += line + '\n'
            } else {
                card.substring(card.length - 1, 1)
                if (card.length > 0) {
                    cards.push(card)
                }
                card = ''
            }
        });

        ref.total = cards.length
        ref.finished = 0

        let interval = 100
        let self = this
        cards.forEach((card, index) => {
            setTimeout(function () {
                card = card.split('\n')
                let row1 = card[0].split(' ')
                let row2 = card[1].split('.')
                let CardId = row2[0].trim()
                let Name = row1[2].trim().replace(']', '')
                let Pack = CardId.split('_')[0]
                let Category = row1[1].trim()
                let CardIdSearch = `${Pack}_${('000' + CardId.split('_')[1]).slice(-3)}`
                let Attribute
                switch (Category) {
                    case '支援者':
                        Category = 'supporter'
                        break;
                    case '能量':
                    case '特殊能量':
                        Category = 'energy'
                        break;
                    case '物品':
                        Category = 'item'
                        break;
                    case '寶可夢道具':
                        Category = 'tool'
                        break;
                    case '競技場':
                        Category = 'stadium'
                        break;

                    case '雷':
                        Category = 'pokemon'
                        Attribute = 'Lightning'
                        break;
                    case '超':
                        Category = 'pokemon'
                        Attribute = 'Psychic'
                        break;
                    case '火':
                        Category = 'pokemon'
                        Attribute = 'Fire'
                        break;
                    case '鬥':
                        Category = 'pokemon'
                        Attribute = 'Fighting'
                        break;
                    case '水':
                        Category = 'pokemon'
                        Attribute = 'Water'
                        break;
                    case '鋼':
                        Category = 'pokemon'
                        Attribute = 'Metal'
                        break;
                    case '惡':
                        Category = 'pokemon'
                        Attribute = 'Darkness'
                        break;
                    case '草':
                        Category = 'pokemon'
                        Attribute = 'Grass'
                        break;
                    case '龍':
                        Category = 'pokemon'
                        Attribute = 'Dragon'
                        break;
                    case '妖':
                        Category = 'pokemon'
                        Attribute = 'Fairy'
                        break;
                    case '無色':
                        Category = 'pokemon'
                        Attribute = 'Colorless'
                        break;

                    default:
                        Category = 'error'
                        break;
                }
                let urlPack = Pack.length < 3 ? `${Pack}-0` : Pack
                urlPack = urlPack.toLowerCase()
                let Image = `https://ptcgworld.blob.core.windows.net/${urlPack}/${CardId}.jpg`

                //  Check if card exist
                let query = self.qs.stringify({
                    filters: {
                        CardId: {
                            $eq: CardId,
                        },
                    },
                }, {
                    encodeValuesOnly: true,
                });
                let headers = {
                    headers: {
                        Authorization:
                            `Bearer ${jwt}`,
                    }
                }
                let url = `${self.baseUrl}/api/cards?${query}`;
                self.axios.get(url, headers)
                    .then(res => {
                        let count = res?.data?.meta?.pagination?.total
                        if (count === 0) {
                            //  Create new card

                            let postBody = {
                                data: {
                                    CardId: CardId,
                                    CardIdSearch: CardIdSearch,
                                    Pack: Pack,
                                    Name: Name,
                                    Category: Category,
                                    Attribute: Attribute,
                                    Image: Image,
                                }
                            }
                            let url = `${self.baseUrl}/api/cards`;
                            self.axios.post(url, postBody, headers)
                                .then(res => {
                                    let newCard = self.cleanUpStrapiResponse(res.data, self)
                                    // console.log(`Added card: ${newCard.CardId} ${newCard.Name}`)
                                    ref.output.push(`已匯入新卡片: ${newCard.CardId} ${newCard.Name}`)
                                })
                                .catch(error => {
                                    // console.log(`Failed to add card: ${CardId} ${Name}`)
                                    // console.log(`${error}`)
                                    // console.log(`${JSON.stringify(postBody)}`)
                                    ref.output.push(`無法匯入卡片: ${CardId} ${Name}\n錯誤: ${error}\nPostBody: ${JSON.stringify(postBody)}`)
                                })
                                .finally(() => {
                                    ref.finished += 1
                                    ref.progress = ref.finished / ref.total

                                    if (ref.finished >= ref.total) {
                                        ref.status = 'done'
                                    }
                                })

                        } else {
                            let dbCard = self.cleanUpStrapiResponse(res.data.data[0], self)
                            if (dbCard.CardId != CardId ||
                                dbCard.CardIdSearch != CardIdSearch ||
                                dbCard.Pack != Pack ||
                                dbCard.Name != Name ||
                                dbCard.Category != Category ||
                                dbCard.Attribute != Attribute ||
                                dbCard.Image != Image) {
                                //  Update image new card

                                // console.log(`${JSON.stringify(dbCard)}`)
                                // console.log(`${dbCard.CardId} ${CardId}`)
                                // console.log(`${dbCard.Name} ${Name}`)
                                // console.log(`${dbCard.Attribute} ${Attribute}`)
                                // console.log(`${dbCard.Category} ${Category}`)
                                // console.log(`${dbCard.Image} ${Image}`)

                                let putBody = {
                                    data: {
                                        CardId: CardId,
                                        CardIdSearch: CardIdSearch,
                                        Pack: Pack,
                                        Name: Name,
                                        Category: Category,
                                        Attribute: Attribute,
                                        Image: Image,
                                    }
                                }
                                let url = `${self.baseUrl}/api/cards/${dbCard.id}`;
                                self.axios.put(url, putBody, headers)
                                    .then(res => {
                                        let updatedCard = self.cleanUpStrapiResponse(res.data, self)
                                        // console.log(`Updated card: ${updatedCard.CardId} ${updatedCard.Name}`)
                                        ref.output.push(`已更新卡片: ${updatedCard.CardId} ${updatedCard.Name}`)
                                    })
                                    .catch(error => {
                                        // console.log(`Failed to update card: ${CardId} ${Name}`)
                                        // console.log(`${error}`)
                                        // console.log(`${JSON.stringify(putBody)}`)
                                        ref.output.push(`無法更新卡片: ${CardId} ${Name}\n錯誤: ${error}\nPostBody: ${JSON.stringify(putBody)}`)
                                    })
                                    .finally(() => {
                                        ref.finished += 1
                                        ref.progress = ref.finished / ref.total

                                        if (ref.finished >= ref.total) {
                                            ref.status = 'done'
                                        }
                                    })
                            } else {
                                // console.log(`${CardId} already exist`)
                                ref.output.push(`已跳過沒有改變的卡片: ${CardId}  ${Name}`)

                                ref.finished += 1
                                ref.progress = ref.finished / ref.total

                                if (ref.finished >= ref.total) {
                                    ref.status = 'done'
                                }
                            }
                        }
                    })
                    .catch(error => {
                        ref.output.push(`未知錯誤: ${CardId} ${Name}\n錯誤: ${error}`)
                    })

                // if (Category === 'error' || Name === undefined || CardId === undefined || Image === undefined) {
                //     console.log(card);
                //     console.log(Name);
                //     console.log(Category);
                // }
            }, index * interval);
        });

    }

    cleanUpStrapiResponse(data, self) {
        if (data === null || data === undefined || typeof data === 'string' || data?.length === 0) {
            return data;
        }

        if (Array.isArray(data)) {
            for (let i = 0; i < data.length; i++) {
                data[i] = self.cleanUpStrapiResponse(data[i], self)
            }
        } else {
            data = self.cleanUpStrapiNode(data, self)
            Object.keys(data).forEach(function (key) {
                data[key] = self.cleanUpStrapiResponse(data[key], self)
            });
        }
        return data
    }

    cleanUpStrapiNode(data, self) {
        let cleanUped = false;
        let cleanUpList = ['attributes', 'data']
        cleanUpList.forEach(key => {
            if (data[key] != undefined) {
                Object.keys(data[key]).forEach(function (innerKey) {
                    data[innerKey] = data[key][innerKey]
                });
                data[key] = undefined
                cleanUped = true;
            }
        });
        if (data['Image']) {
            // data['Image'] = 'https://lineimg.omusic.com.tw/img/album/2551883.jpg?v=20200722015255'
        }

        if (cleanUped) {
            data = self.cleanUpStrapiNode(data, self)
        }

        return data;
    }

    generateDeckCode(self) {
        let slug = self.generateUID()
        return slug
    }

    /**
     * It generates a random number between 0 and 46656, converts it to base 36, and then pads it with
     * zeros to make it 3 characters long. It does this twice, and then concatenates the two strings
     * together.
     * 
     * The result is a 6 character string that is unique enough for our purposes.
     * @returns A string of 6 characters.
     */
    generateUID() {
        var firstPart = (Math.random() * 46656) | 0;
        var secondPart = (Math.random() * 46656) | 0;
        firstPart = ('000' + firstPart.toString(36)).slice(-3);
        secondPart = ('000' + secondPart.toString(36)).slice(-3);
        return firstPart + secondPart;
    }

}

module.exports = StrapiController;