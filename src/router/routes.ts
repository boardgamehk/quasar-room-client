import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    redirect: () => {
      return { path: '/room' }
    },
  },
  {
    path: '/room',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/RoomManage.vue') }],
  },
  {
    path: '/deck',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/DeckBuilder.vue') }],
  },
  {
    path: '/card',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/CardBuilder.vue') }],
  },
  {
    path: '/game',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/GameClient.vue') }],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
