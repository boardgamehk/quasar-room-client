FROM node:16
COPY . /quasar-app
WORKDIR /quasar-app
RUN npm install
RUN npx quasar build
WORKDIR /quasar-app/dist/spa
RUN npm install http-server -g
EXPOSE 8080
CMD ["sleep", "infinity"]
